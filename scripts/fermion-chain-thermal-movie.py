#!/usr/bin/env python3
import numpy
import pickle
from matplotlib import pyplot
from matplotlib import animation
import matplotlib.colors
import matplotlib.cm
import colorcet

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True

with open('fermion-chain-thermal-states-lambda0.5-V0.1-L_A6-beta_A0-L_B6.npy', 'rb') as f:
    data = pickle.load(f)

norm =  matplotlib.colors.Normalize(vmin=0, vmax=1)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.fire)

n_min = 1.0
n_max = 0.0
i = 0
for t in sorted(data.keys()):
    print(t)
    fig = pyplot.figure(figsize=(4,2), dpi=300)
    pyplot.subplots_adjust(hspace=0.50)
    t_str = str(int(t))
    t_str = (r'\phantom{0}' * (4 - len(t_str))) + t_str
    #title = ax.text(0.5,1.05,rf"$t = {t_str}$", 
    #                size=pyplot.rcParams["axes.titlesize"],
    #                ha="center", transform=ax.transAxes, )
    pyplot.title(rf"$\beta_\mathcal{{A}} = 0, \beta_\mathcal{{B}} = 10$, t = {t_str}")
    im    = pyplot.imshow(data[t], animated=True, origin='lower', cmap=colorcet.cm.fire, norm=norm)
    cb = pyplot.colorbar(mappable=mapper, label=r"Single-Site Density $\langle n_i \rangle$")
    n_min = min(n_min, numpy.min(data[t]))
    n_max = max(n_max, numpy.max(data[t]))
    print(n_min, n_max)
    pyplot.xlabel(r"Site Index $i$")
    pyplot.xticks([1,3,5,7,9,11], [2,4,6,8,10,12])
    pyplot.ylabel(r"")
    pyplot.yticks([],[])
    #pyplot.ylabel(r"Initial State of Subsystem B $|\Psi\rangle_\mathcal{B}$")
    #pyplot.yticks([0,4,9,14,19], [r"$|0\rangle$", r"$|4\rangle$", r"$|9\rangle$", r"$|14\rangle$", r"$|19\rangle$" ])
    pyplot.savefig(f"frames/fermion-chain-thermal-states-lambda0.5-V0.1-L_A6-beta_A0-L_B6-{i}.png")
    pyplot.close()
    i = i + 1

    #  ffmpeg -f image2 -framerate 60 -i fermion-chain-thermal-states-lambda0.5-V0.1-L_A6-beta_A0-L_B6-%d.png -pix_fmt yuv420p -vcodec h264 -r 60  fermion-chain-thermal-states-lambda0.5-V0.1-L_A6-beta_A0-L_B6.mp4
