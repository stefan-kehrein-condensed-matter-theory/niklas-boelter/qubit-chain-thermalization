#!/usr/bin/env python3
import scipy.special
import numpy
import pickle
from collections import defaultdict

length_A = 6
length_B = 6
beta_A = 0

dim_B_half_filling = int(scipy.special.binom(length_B, length_B//2))

results = {} #defaultdict(lambda: numpy.zeros((dim_B_half_filling, length_A + length_B)))

for beta_B in [10]:
    filename = f"fermion-chain-thermal-states-lambda0.5-V0.1-L_A{length_A}-beta_A{beta_A}-L_B{length_B}-beta_B{beta_B}.txt"
    with open(filename) as f:
        for line in f:
            line = list(map(float, line.strip().split(" ")))
            t = line.pop(0)
            if not t in results:
                results[t] = numpy.zeros((1, length_A + length_B))
            for i, density in zip(range(0, length_A + length_B), line):
                results[t][0, i] = density
            print(t, '', end='')
        print()
with open(f"fermion-chain-thermal-states-lambda0.5-V0.1-L_A{length_A}-beta_A{beta_A}-L_B{length_B}.npy", "wb") as f:
    pickle.dump(results, f)
