#!/usr/bin/env python3
# -*- coding: utf8 -*-
from os.path import expanduser
import colorcet
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import sqlite3

L = 12

L_A = L // 2
L_B = L // 2

beta_A = 0.0
beta_B = 10.0

norm = matplotlib.colors.Normalize(vmin=0, vmax=10)
mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

pyplot.rcParams['text.usetex'] = True
pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
pyplot.rcParams['figure.autolayout'] = True
home = expanduser("~")

con = sqlite3.connect(home + "/fermion-chain-thermalization.db")
con.enable_load_extension(True)
con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
con.enable_load_extension(False)
c = con.cursor()

data = numpy.array(list(zip(*(c.execute("""SELECT t, E_A, E_B FROM results_a4acfdf WHERE L_A = ? AND L_B = ? AND beta_A = ? AND beta_B = ? ORDER BY t;""", (L_A, L_B, beta_A, beta_B))))))

pyplot.plot(data[0], data[1], label=r"$E_A$")
pyplot.plot(data[0], data[2], label=r"$E_B$")

pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\beta_A = {beta_A}, \beta_B = {beta_B}$")
pyplot.ylabel(r"Subsystem Energy $E$")
pyplot.xlabel(r"Time $t$")
pyplot.legend()
pyplot.savefig(f"fermion-chain-thermalization-energy-canonical-L_A{L_A}-L_B{L_B}-beta_A{beta_A}-beta_B{beta_B}.png", format='png', dpi=300, transparent=False)
pyplot.clf()

data = numpy.array(list(zip(*(c.execute("""SELECT t, S_A, S_B FROM results_a4acfdf WHERE L_A = ? AND L_B = ? AND beta_A = ? AND beta_B = ? ORDER BY t;""", (L_A, L_B, beta_A, beta_B))))))

pyplot.plot(data[0], data[1], label=r"$S_A$")
pyplot.plot(data[0], data[2], label=r"$S_B$")

pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\beta_A = {beta_A}, \beta_B = {beta_B}$")
pyplot.ylabel(r"Subsystem Entropy $S$")
pyplot.xlabel(r"Time $t$")
pyplot.legend()
pyplot.savefig(f"fermion-chain-thermalization-entropy-canonical-L_A{L_A}-L_B{L_B}-beta_A{beta_A}-beta_B{beta_B}.png", format='png', dpi=300, transparent=False)
pyplot.clf()


data = numpy.array(list(zip(*(c.execute("""SELECT t, N_B - N_A, N_A_err FROM results_a4acfdf WHERE L_A = ? AND L_B = ? AND beta_A = ? AND beta_B = ? AND t > 0 ORDER BY t;""", (L_A, L_B, beta_A, beta_B))))))

pyplot.fill_between(data[0], data[1]+data[2], data[1]-data[2], alpha=0.5, label=r"$\Delta N \pm \sigma_{\Delta N}$")
pyplot.plot(data[0], data[1], linewidth=1.0, label=r"$\Delta N$")


pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\beta_A = {beta_A}, \beta_B = {beta_B}$")
pyplot.ylabel(r"Particle Number Difference $\Delta N = N_B - N_A$")
pyplot.xlabel(r"Time $t$")
pyplot.legend()
pyplot.savefig(f"fermion-chain-thermalization-particle-number-canonical-L_A{L_A}-L_B{L_B}-beta_A{beta_A}-beta_B{beta_B}.png", format='png', dpi=300, transparent=False)
