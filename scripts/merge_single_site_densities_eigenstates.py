#!/usr/bin/env python3
import scipy.special
import numpy
import pickle
from collections import defaultdict

length_A = 6
length_B = 6
eigenstate_A = 0

dim_B_half_filling = int(scipy.special.binom(length_B, length_B//2))

results = {} #defaultdict(lambda: numpy.zeros((dim_B_half_filling, length_A + length_B)))

for eigenstate_B in range(0, dim_B_half_filling):
    filename = f"fermion-chain-eigenstates-lambda0.5-V0.1-L_A{length_A}-n_A{eigenstate_A}-L_B{length_B}-n_B{eigenstate_B}.txt"
    with open(filename) as f:
        for line in f:
            line = list(map(float, line.strip().split(" ")))
            t = line.pop(0)
            if not t in results:
                results[t] = numpy.zeros((dim_B_half_filling, length_A + length_B))
            for i, density in zip(range(0, length_A + length_B), line):
                results[t][eigenstate_B, i] = density
            print(t, '', end='')
        print()
with open(f"fermion-chain-eigenstates-lambda0.5-V0.1-L_A{length_A}-n_A{eigenstate_A}-L_B{length_B}.npy", "wb") as f:
    pickle.dump(results, f)
