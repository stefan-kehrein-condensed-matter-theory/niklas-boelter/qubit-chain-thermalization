#!/usr/bin/env python3
# -*- coding: utf8 -*-
from os.path import expanduser
import colorcet
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as pyplot
import numpy
import sqlite3

L = 16

L_A = L // 2
L_B = L // 2

n_A = 0
for n_B in [35,]:

    norm = matplotlib.colors.Normalize(vmin=0, vmax=10)
    mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=colorcet.cm.CET_R2)

    pyplot.rcParams['text.usetex'] = True
    pyplot.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
    pyplot.rcParams['figure.autolayout'] = True
    home = expanduser("~")

    con = sqlite3.connect(home + "/fermion-chain-thermalization-eigenstates.db")
    con.enable_load_extension(True)
    con.execute("select load_extension('" + home + "/qubit-chain-tfd/libsqlitefunctions.so')")
    con.enable_load_extension(False)
    c = con.cursor()

    data = numpy.array(list(zip(*(c.execute("""SELECT t, E_A, E_B FROM results_a4acfdf WHERE lambda = 0.5 AND V = 0.1 AND L_A = ? AND L_B = ? AND L_A = L_B AND eigenstate_A = ? AND eigenstate_B = ? ORDER BY t;""", (L_A, L_B, n_A, n_B))))))

    if len(data) == 0:
        continue

    pyplot.plot(data[0], data[1], label=r"$E_A$")
    pyplot.plot(data[0], data[2], label=r"$E_B$")

    pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\rho_A = |{n_A}\rangle\langle {n_A}|, \rho_B = |{n_B}\rangle\langle {n_B}| $")
    pyplot.ylabel(r"Subsystem Energy $E$")
    pyplot.xlabel(r"Time $t$")
    pyplot.legend()
    pyplot.savefig(f"fermion-chain-thermalization-energy-eigenstates-L_A{L_A}-L_B{L_B}-n_A{n_A}-n_B{n_B}.png", format='png', dpi=300, transparent=False)
    pyplot.clf()

    data = numpy.array(list(zip(*(c.execute("""SELECT t, S_A, S_B FROM results_a4acfdf WHERE lambda = 0.5 AND V = 0.1 AND L_A = ? AND L_B = ? AND L_A = L_B AND eigenstate_A = ? AND eigenstate_B = ? ORDER BY t;""", (L_A, L_B, n_A, n_B))))))

    pyplot.plot(data[0], data[1], label=r"$S_A$")
    pyplot.plot(data[0], data[2], '--', label=r"$S_B$")

    pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\rho_A = |{n_A}\rangle\langle {n_A}|, \rho_B = |{n_B}\rangle\langle {n_B}| $")
    pyplot.ylabel(r"Subsystem Entropy $S$")
    pyplot.xlabel(r"Time $t$")
    pyplot.legend()
    pyplot.savefig(f"fermion-chain-thermalization-entropy-eigenstates-L_A{L_A}-L_B{L_B}-n_A{n_A}-n_B{n_B}.png", format='png', dpi=300, transparent=False)
    pyplot.clf()

    data = numpy.array(list(zip(*(c.execute("""SELECT t, N_B - N_A, N_A_err FROM results_a4acfdf WHERE lambda = 0.5 AND V = 0.1 AND L_A = ? AND L_B = ? AND L_A = L_B AND eigenstate_A = ? AND eigenstate_B = ? AND t > 0.0 ORDER BY t;""", (L_A, L_B, n_A, n_B))))))

    data[2][0] = 0.0


    pyplot.fill_between(data[0], data[1]+data[2], data[1]-data[2], alpha=0.5, label=r"$\Delta N \pm \sigma_{\Delta N}$")
    pyplot.plot(data[0], data[1], linewidth=1.0, label=r"$\Delta N$")


    pyplot.title(rf"Spinless Fermion Chain, $L_A = {L_A}, L_B = {L_B}$,  $\rho_A = |{n_A}\rangle\langle {n_A}|, \rho_B = |{n_B}\rangle\langle {n_B}| $")
    pyplot.ylabel(r"Particle Number Difference $\Delta N = N_B - N_A$")
    pyplot.xlabel(r"Time $t$")
    pyplot.legend()
    pyplot.savefig(f"fermion-chain-thermalization-particle-number-eigenstates-L_A{L_A}-L_B{L_B}-n_A{n_A}-n_B{n_B}.png", format='png', dpi=300, transparent=False)
    pyplot.clf()
