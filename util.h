/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef QUBIT_CHAIN_UTIL_H
#define QUBIT_CHAIN_UTIL_H

#include <time.h>
#include <inttypes.h>

#ifdef USE_INTEL_MKL
#define MKL_Complex16 double _Complex
#include <mkl.h>
#else
#include <lapacke.h>
#include <cblas.h>
#endif

#ifdef USE_LIBFLAME
char*         FLA_Get_AOCL_Version( void );
#endif

#undef min
#define min( x, y ) ( (x) < (y) ? (x) : (y) )

#undef max
#define max( x, y ) ( (x) > (y) ? (x) : (y) )

extern clock_t start;
extern int argv_len; // Used for status reporting
extern char *argv_ptr;
extern char *status_prefix; // Prefix string for status reporting

double calculate_Renyi_entropy(const double *eigenvalues, int M, double alpha);
double calculate_vonNeumann_entropy(const double *eigenvalues, int M);
double calculate_min_entropy(const double *eigenvalues, int M);
int calculate_momentum(long long int x,  int L);
int parity_transformation(long long int x, int L);
double inverse_participation_ratio(const double *state, long long dim, long long stride);
double participation_Renyi_entropy(const double *state, long long dim,
  long long stride, double alpha);
double consecutive_level_spacing_ratio(const double *H_eigenvalues, const int *H_popcount,
    long long dim, int filling);

void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues);
void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues);
void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues);
void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues);
void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues);

void report_status(const char *str);
void report_elapsed_time(void);
void print_version(void);

void check_normalized(const double *V, long long int N);
void check_normalized_complex(const double _Complex *V, long long int N);
void check_unitary(const double _Complex *U, long long int N);
void check_hermitian(const double _Complex *rho, long long int N);
void check_symmetric(const double *rho, long long int N);
void show_checksum(const void *data, size_t size);
double find_temperature(double *hamiltonian_spectrum, int dim, double E);

/*
 * Code Taken from
 * Henry S. Warren, “Hacker’s Delight”, Addison-Wesley Professional, 2013, Second Edition 
 */

inline uint32_t fallback_pext_u32(uint32_t value, uint32_t mask)
{
    int i;
    uint32_t mk = (~mask << 1);                    // we will count 0's to the right
    uint32_t mp;
    uint32_t mv;
    uint32_t tx;
    value = (value & mask);                         // clear irrelevant bits

    for (i = 0; i < 5; ++i)                         // log_2 of the bit size (here, 32 bits)
    {
        mp     = mk ^ (mk <<  1);                   // parallel suffix
        mp     = mp ^ (mp <<  2);
        mp     = mp ^ (mp <<  4);
        mp     = mp ^ (mp <<  8);
        mp     = mp ^ (mp << 16);
        mv     = (mp & mask);                       // bits to move
        mask   = ((mask ^ mv) | (mv >> (1 << i)));  // compress mask
        tx      = (value & mv);
        value = ((value ^ tx) | (tx >> (1 << i)));    // compress value
        mk    &= ~mp;
    }
    return value;
}


inline uint32_t fallback_pdep_u32(uint32_t value, uint32_t mask)
{
   uint32_t m0, mk, mp, mv, tx;
   uint32_t array[5];
   int i;

   m0 = mask;              // Save original mask.
   mk = ~mask << 1;        // We will count 0's to right.

   for (i = 0; i < 5; i++) {
      mp = mk ^ (mk << 1);              // Parallel suffix.
      mp = mp ^ (mp << 2);
      mp = mp ^ (mp << 4);
      mp = mp ^ (mp << 8);
      mp = mp ^ (mp << 16);
      mv = mp & mask;                      // Bits to move.
      array[i] = mv;
      mask = (mask ^ mv) | (mv >> (1 << i));  // Compress m.
      mk = mk & ~mp;
   }

   for (i = 4; i >= 0; i--) {
      mv = array[i];
      tx = value << (1 << i);
      value = (value & ~mv) | (tx & mv);
//    value = ((value ^ t) & mv) ^ value;           // Alternative for above line.
   }
   return value & m0;       // Clear out extraneous bits.
}


inline uint64_t fallback_pext_u64(uint64_t value, uint64_t mask)
{
    int i;
    uint64_t mk = (~mask << 1);                    // we will count 0's to the right
    uint64_t mp;
    uint64_t mv;
    uint64_t tx;
    value = (value & mask);                         // clear irrelevant bits

    for (i = 0; i < 6; ++i)                         // log_2 of the bit size (here, 32 bits)
    {
        mp     = mk ^ (mk <<  1);                   // parallel suffix
        mp     = mp ^ (mp <<  2);
        mp     = mp ^ (mp <<  4);
        mp     = mp ^ (mp <<  8);
        mp     = mp ^ (mp << 16);
        mp     = mp ^ (mp << 32);
        mv     = (mp & mask);                       // bits to move
        mask   = ((mask ^ mv) | (mv >> (1 << i)));  // compress mask
        tx      = (value & mv);
        value = ((value ^ tx) | (tx >> (1 << i)));    // compress value
        mk    &= ~mp;
    }
    return value;
}


inline uint64_t fallback_pdep_u64(uint64_t value, uint64_t mask)
{
   uint64_t m0, mk, mp, mv, tx;
   uint64_t array[6];
   int i;

   m0 = mask;              // Save original mask.
   mk = ~mask << 1;        // We will count 0's to right.

   for (i = 0; i < 6; i++) {
      mp = mk ^ (mk << 1);              // Parallel suffix.
      mp = mp ^ (mp << 2);
      mp = mp ^ (mp << 4);
      mp = mp ^ (mp << 8);
      mp = mp ^ (mp << 16);
      mp = mp ^ (mp << 32);
      mv = mp & mask;                      // Bits to move.
      array[i] = mv;
      mask = (mask ^ mv) | (mv >> (1 << i));  // Compress m.
      mk = mk & ~mp;
   }

   for (i = 5; i >= 0; i--) {
      mv = array[i];
      tx = value << (1 << i);
      value = (value & ~mv) | (tx & mv);
//    value = ((value ^ t) & mv) ^ value;           // Alternative for above line.
   }
   return value & m0;       // Clear out extraneous bits.
}

#endif

