/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef FREE_FERMION_COUNT_H
#define FREE_FERMION_COUNT_H

#include <complex.h>

void print_state(long long int n, int L, int print_header);

void show_TFD(const double _Complex *TFD, double partition_sum, long long int dim);

void show_hamiltonian(const double *hamiltonian, long long int dim, int L);

void show_rho_X(const double *rho_X, int L_X);
void show_rho_XY(const double _Complex *rho_XY, int L_X, int L_Y);
void show_rho_eigenvalues(const double *rho_eigenvalues, int dim);
void show_system_layout(int mask_A, int mask_C, int L);
void show_U(const double _Complex *U, int dim);
void show_O(const double *O, int dim);
void show_psi(const double _Complex *U, int dim);


#endif

