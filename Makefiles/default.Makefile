CC=clang
CFLAGS=-O2 -Wall
CPPFLAGS=-isystem/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/System/Library/Frameworks/Accelerate.framework/Versions/Current/Frameworks/vecLib.framework/Headers/ -isystem/usr/local/opt/lapack/include -isystem/usr/include/lapacke/ -isystem/usr/local/opt/sqlite/include
LDFLAGS=-lblas -llapack -lsqlite3 -lm

include Makefile.in
