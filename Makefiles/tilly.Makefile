CC=clang
CFLAGS=-O2 -std=gnu99 -pedantic -Wall -march=native -mkl -DMKL_ILP64
CPPFLAGS=-I/scratch/AG-Kehrein/libbsd/include -I/scratch/AG-Kehrein/libsqlite3/include -DUSE_INTEL_MKL
LDFLAGS=-L/scratch/AG-Kehrein/libbsd/lib -L/scratch/AG-Kehrein/libsqlite3/lib -lsqlite3 -lbsd -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lm

include Makefile.in
