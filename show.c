#include "show.h"
#include "util.h"
#include <stdio.h>
#include <stdint.h>
#include <math.h>

void print_state(long long int n, int L, int print_header)
{
    int i;
    if(print_header)
    {
        for(i = 0; i < L; i++)
        {
            printf("|");
            printf("%i", i+1);
        }
        printf("|\n");
    }

    for(i = 0; i < L; i++)
    {
        printf("|");
        if(n & (1<<i))
        {
            printf("\033[34m●\033[39m");
        }
        else
        {
            printf(" ");
        }
    }
    printf("|");
    if(print_header)
    {
        printf("\n");
    }
}

void show_TFD(const double _Complex *TFD, double partition_sum, long long int dim)
{
    int i,j;

    printf("============= TFD Vector =============\n\n  ");
    printf(" ");
    for(j = 0; j < (dim*18-1); j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(cabs(TFD[i*dim+j]) < 1E-12)
            {
                printf("|\033[39m %6.3lf+i*%6.3lf \033[39m", creal(TFD[i*dim+j])/sqrt(partition_sum), cimag(TFD[i*dim+j])/sqrt(partition_sum));
            }
            else if(cabs(TFD[i*dim+j]) < 1E-3)
            {
                printf("|\033[37m %6.3lf+i*%6.3lf \033[39m", creal(TFD[i*dim+j])/sqrt(partition_sum), cimag(TFD[i*dim+j])/sqrt(partition_sum));
            }
            else
            {
                printf("|\033[31m %6.3lf+i*%6.3lf \033[39m", creal(TFD[i*dim+j])/sqrt(partition_sum), cimag(TFD[i*dim+j])/sqrt(partition_sum));
            }
        }
        printf("|\n   ");
        for(j = 0; j < (dim*18-1); j++)
        {
            printf("-");
        }
        printf("\n  ");
    }
    printf("\n");
}

void show_hamiltonian(const double *hamiltonian, long long int dim, int L)
{
    int i,j;

    printf("============ Hamiltonian ============\n\n   ");

    for(j = 0; j < dim*8-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(hamiltonian[j + i*dim] != 0.0)
            {
                printf("|\033[31m %5.2f \033[39m", hamiltonian[j + i*dim]);
            }
            else
            {
                printf("|\033[37m %5.2f \033[39m", hamiltonian[j + i*dim]);
            }
            
        }
        printf("|    ");
        print_state(i, L, 0);
        printf(" -> ");
        for(j = 0; j < dim; j++)
        {
            if(hamiltonian[j + i*dim] != 0.0)
            {
                if(hamiltonian[j + i*dim] > 0)
                {
                    printf(" -");
                }
                else
                {
                    printf(" +");
                }
                printf("[");
                print_state(j, L, 0);
                printf("]");
            }
        }
        printf("\n   ");
        for(j = 0; j < dim*8-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n");
}


void show_rho_X(const double *rho_X, int L_X)
{
    int i,j;
    int dim = 1 << L_X;

    printf(" ");
    for(j = 0; j < 9*dim-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(fabs(rho_X[j + i * dim])>1E-12)
            {
                printf("|\033[31m %6.2lf \033[39m", rho_X[j + i * dim]);
            }
            else
            {
                printf("|\033[37m %6.2lf \033[39m", rho_X[j + i * dim]);
            }
            
        }
        printf("|    ");
        print_state(i, L_X, 0);
        printf("\n   ");
        for(j = 0; j < 9*dim-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n  ");
}


void show_rho_XY(const double _Complex *rho_XY, int L_X, int L_Y)
{
    int i,j;

    int dim_X = 1 << L_X;
    int dim_Y = 1 << L_Y;

    int dim = dim_X * dim_Y;

    printf(" ");
    for(j = 0; j < 17*dim-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(creal(rho_XY[j + i*dim]) > 1E-12)
            {
                printf("|\033[32m %6.3lf\033[39m", creal(rho_XY[j + i*dim]));
            }
            else if(creal(rho_XY[j + i*dim]) < -1E-12)
            {
                printf("|\033[31m %6.3lf\033[39m", creal(rho_XY[j + i*dim]));
            }
            else
            {
                printf("|\033[37m       \033[39m");
            }


            if(cimag(rho_XY[j + i*dim]) > 1E-12)
            {
                printf("\033[32m+i%6.3lf \033[39m", cimag(rho_XY[j + i*dim]));
            }
            else if(cimag(rho_XY[j + i*dim]) < -1E-12)
            {
                printf("\033[31m-i%6.3lf \033[39m", -cimag(rho_XY[j + i*dim]));
            }
            else
            {
                printf("\033[37m         \033[39m");
            }
            
        }
        printf("|    ");
        
        print_state(i%dim_X, L_X, 0);
        printf("⊗");
        print_state(i/dim_X, L_Y, 0);
        printf("\n   ");
        for(j = 0; j < 17*dim-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n  ");
}


void show_rho_eigenvalues(const double *rho_eigenvalues, int dim)
{
    int i;
    double norm;

    norm = 0.0;
    for(i = 0; i < dim; i++)
    {
        norm += rho_eigenvalues[i];
    }
    printf("Trace: %g\n  ", norm);

    printf("Eigenvalues: ");
    for(i = 0; i < dim; i++)
    {
        if(fabs(rho_eigenvalues[i]) > 1E-12)
        {
            printf("%g ", rho_eigenvalues[i]);
        }
    }
    printf("\n");
}


void show_system_layout(int mask_A, int mask_C, int L)
{
    int i;
    int mask_B = ((1<<L) - 1) - mask_A;
    int mask_D = ((1<<L) - 1) - mask_C;

    printf("====== System Layout ======\n\n");

    for(i = 0; i < (13 - L/2); i++)
    {
        printf(" ");
    }

    for(i = 1; i < (1<<L); i = i<<1)
    {
        if(mask_A & i)
        {
            printf("\033[34mA\033[39m");
        }
        else if(mask_B & i)
        {
            printf("\033[35mB\033[39m");
        }
    }
    printf("\n");

    for(i = 0; i < (13 - L/2); i++)
    {
        printf(" ");
    }

    for(i = 1; i < (1<<L); i = i<<1)
    {
        if(mask_C & i)
        {
            printf("\033[34mC\033[39m");
        }
        else if(mask_D & i)
        {
            printf("\033[35mD\033[39m");
        }
    }
    printf("\n");
}


void show_U(const double _Complex *U, int dim)
{
    int i,j;

    printf(" ");
    for(j = 0; j < 17*dim-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(creal(U[j + i*dim]) > 1E-12)
            {
                printf("|\033[32m %6.3lf\033[39m", creal(U[j + i*dim]));
            }
            else if(creal(U[j + i*dim]) < -1E-12)
            {
                printf("|\033[31m %6.3lf\033[39m", creal(U[j + i*dim]));
            }
            else
            {
                printf("|\033[37m       \033[39m");
            }


            if(cimag(U[j + i*dim]) > 1E-12)
            {
                printf("\033[32m+i%6.3lf \033[39m", cimag(U[j + i*dim]));
            }
            else if(cimag(U[j + i*dim]) < -1E-12)
            {
                printf("\033[31m-i%6.3lf \033[39m", -cimag(U[j + i*dim]));
            }
            else
            {
                printf("\033[37m         \033[39m");
            }
            
        }
        printf("|");
        printf("\n   ");
        for(j = 0; j < 17*dim-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n  ");
}


void show_O(const double *O, int dim)
{
    int i,j;

    printf(" ");
    for(j = 0; j < 9*dim-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(fabs(O[j + i * dim])>1E-12)
            {
                printf("|\033[31m %6.2lf \033[39m", O[j + i * dim]);
            }
            else
            {
                printf("|\033[37m %6.2lf \033[39m", O[j + i * dim]);
            }
            
        }
        printf("|");
        printf("\n   ");
        for(j = 0; j < 9*dim-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n  ");
}


void show_psi(const double _Complex *U, int dim)
{
    int i,j;

    printf(" ");
    for(j = 0; j < 17*dim-1; j++)
    {
        printf("-");
    }
    printf("\n  ");

    for(i = 0; i < 1; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(creal(U[j + i*dim]) > 1E-12)
            {
                printf("|\033[32m %6.3lf\033[39m", creal(U[j + i*dim]));
            }
            else if(creal(U[j + i*dim]) < -1E-12)
            {
                printf("|\033[31m %6.3lf\033[39m", creal(U[j + i*dim]));
            }
            else
            {
                printf("|\033[37m       \033[39m");
            }


            if(cimag(U[j + i*dim]) > 1E-12)
            {
                printf("\033[32m+i%6.3lf \033[39m", cimag(U[j + i*dim]));
            }
            else if(cimag(U[j + i*dim]) < -1E-12)
            {
                printf("\033[31m-i%6.3lf \033[39m", -cimag(U[j + i*dim]));
            }
            else
            {
                printf("\033[37m         \033[39m");
            }
            
        }
        printf("|");
        printf("\n   ");
        for(j = 0; j < 17*dim-1; j++)
        {
            printf("-");
        }
        printf("\n  ");
        
    }
    printf("\n  ");
}
