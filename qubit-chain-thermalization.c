#include "util.h"
#include "show.h"
#include <sqlite3.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include <sysexits.h>
#include <errno.h>
#include <sys/file.h>

void usage(const char *argv0);
void get_hamiltonian(double _Complex *hamiltonian, double *hamiltonian_spectrum, int length, int periodic_boundary);
int find_blocks(double _Complex *H, int *block_structure, long long int size);
void find_blocks_recursion(double _Complex *H, int *block_structure, long long int size, int row, int index);

#ifdef STORE_RESULT
#define TABLENAME "results_" GIT_DESCRIBE
int find_results(void);
void store_results(void);
#endif


static clock_t init;

static double lambda;
static double V;
static int length_A;
static int length_B;

static double beta_A;
static double beta_B;

static double t_min;
static double delta_t;
static double t_max;

static double _Complex *hamiltonian;
static double _Complex *hamiltonian_A;
static double _Complex *hamiltonian_B;

static double *hamiltonian_spectrum;
static double *hamiltonian_A_spectrum;
static double *hamiltonian_B_spectrum;


static int *hamiltonian_popcount;
static int *hamiltonian_A_popcount;
static int *hamiltonian_B_popcount;

static double _Complex *rho;
static double _Complex *T;
static double _Complex *tmp;
static double _Complex *rho_A;
static double _Complex *rho_B;
static double *rho_A_spectrum;
static double *rho_B_spectrum;

double t, S_A, E_A, N_A, N_A_err, S_B, E_B, N_B, N_B_err;

void usage(const char *argv0)
{
    printf("Usage: %s lambda V L_A beta_A L_B beta_B t_min delta_t t_max\n", argv0);
    printf("Calculates thermalization of bipartite 1D chain\n\n");

#ifdef HARDCORE_BOSONS
    printf("System: 1D tight-binding chain with hardcore bosons\n");
    printf("h = - 0.5 \\sum_i (b^†_i b_{i+1} + lambda * b^†_i b_{i+2} + h.c.) "
           "+ V \\sum_i b^†_i b_{i} b^†_{i+1} b_{i+1} \n\n");
#else
    printf("System: 1D tight-binding chain with spinless fermions\n");
    printf("h = - 0.5 \\sum_i (c^†_i c_{i+1} + lambda * c^†_i c_{i+2} + h.c.) "
           "+ V \\sum_i c^†_i c_{i} c^†_{i+1} c_{i+1} \n\n");
#endif

    printf("lambda\t Next Nearest Neighbor Hopping\n");
    printf("V\t Interaction\n");
    printf("L_X\t System Size of Partition X\n");
    printf("beta_X\t Inverse Temperature of Partition X\n");
    printf("t\t Time from t_min to t_max in steps of delta_t\n");

    print_version();
}

int main(int argc, char *argv[])
{
    init = clock();

    int i,j;
    char buf[2048];
    char filename[2048];
    FILE *output_file;

    int dim, dim_A, dim_B, dim_A_half_filling, dim_B_half_filling;
    int n,m;
    int n_A, n_B;
    int row_A, col_A;
    int row_B, col_B;
    int diag_A, diag_B;
    int row_AB;
    int col_AB;
    int number_of_steps;
    double _Complex prefactor;
    double partition_sum_A;
    double partition_sum_B;
    double min_energy;

    // (0) Command Line Arguments

    if(argc != 10) {
        usage(argv[0]);
        exit(EX_USAGE);
    }

    lambda   = atof(argv[1]);
    V        = atof(argv[2]);
    length_A = atoi(argv[3]);
    beta_A   = atof(argv[4]);
    length_B = atoi(argv[5]);
    beta_B   = atof(argv[6]);
    t_min    = atof(argv[7]);
    delta_t  = atof(argv[8]);
    t_max    = atof(argv[9]);

    if(length_A < 1 || length_A > 31 || length_B < 1 || length_B > 31)
    {
        fprintf(stderr, "\033[31mParameter Error.\033[39m\n");
        exit(EX_USAGE);
    }

    #ifdef USE_INTEL_MKL
    mkl_set_num_threads(1);
    #endif

    argv_len = 0;
    for(i = 0; i < argc; i++)
    {
        argv_len += strlen(argv[i]) + 1;
    }
    argv_ptr = &argv[0][0];

    snprintf(filename, sizeof(filename), "fermion-chain-thermal-states-lambda%g-V%g-L_A%i-beta_A%g-L_B%i-beta_B%g.txt",
             lambda, V, length_A, beta_A, length_B, beta_B);
    output_file = fopen(filename, "a");
    if(!output_file)
    {
        fprintf(stderr, "\n\nCould not open file %s for writing: Error %i\n", filename, errno);
        exit(EX_CANTCREAT);
    }

    if(flock(fileno(output_file), LOCK_EX | LOCK_NB))
    {
        fprintf(stderr, "\n\nCould not lock file %s for writing: Error %i\n", filename, errno);
        exit(EX_CANTCREAT);
    }

    printf(" Welcome to Qubit Chain Thermalization\n\n");
    printf("  h = - 0.5 \\sum_i (\033[34mc^†_i c_{i+1}\033[39m + lambda * \033[34mc^†_i c_{i+2}\033[39m + h.c.) "
           "+ V \\sum_i \033[34mc^†_i c_{i} c^†_{i+1} c_{i+1}\033[39m \n\n");
    printf("  \033[33mlambda:   %g\033[39m\n", lambda);
    printf("  \033[33mV:        %g\033[39m\n\n", V);
    printf("  \033[33mL_A:      %i\033[39m\n", length_A);
    printf("  \033[33mbeta_A:   %g\033[39m\n\n", beta_A);
    printf("  \033[33mL_B:      %i\033[39m\n", length_B);
    printf("  \033[33mbeta_B:   %g\033[39m\n\n", beta_B);
    // Each bit corresponds to a site, so we need to loop over all
    // L-bit integers: 0 ... 2^L - 1 where 2^L == 1 << L

    dim   = 1 << (length_A + length_B);
    dim_A = 1 << length_A;
    dim_B = 1 << length_B;

    dim_A_half_filling = 0;
    for(i = 0; i < dim_A; i++)
    {
        if(__builtin_popcount(i) == length_A/2)
        {
            dim_A_half_filling++;
        }
    }

    dim_B_half_filling = 0;
    for(i = 0; i < dim_B; i++)
    {
        if(__builtin_popcount(i) == length_B/2)
        {
            dim_B_half_filling++;
        }
    }

    // (1) Memory Allocation

    printf("\033[33mMemory Required: >%5.3lf GB\033[39m\n\n", 
        (sizeof(double _Complex) * (3 * dim + 1) * dim)/1.0E9);

    hamiltonian   = calloc(dim * dim, sizeof(double _Complex));
    hamiltonian_A = calloc(dim_A * dim_A, sizeof(double _Complex));
    hamiltonian_B = calloc(dim_B * dim_B, sizeof(double _Complex));

    hamiltonian_spectrum                = calloc(dim, sizeof(double));
    hamiltonian_A_spectrum              = calloc(dim_A, sizeof(double));
    hamiltonian_B_spectrum              = calloc(dim_B, sizeof(double));

    rho     = calloc(dim * dim, sizeof(double _Complex));
    T       = calloc(dim * dim, sizeof(double _Complex));
    tmp     = calloc(dim * dim, sizeof(double _Complex));

    rho_A = calloc(dim_A * dim_A, sizeof(double _Complex));
    rho_A_spectrum = calloc(dim_A, sizeof(double _Complex));

    rho_B = calloc(dim_B * dim_B, sizeof(double _Complex));
    rho_B_spectrum = calloc(dim_B, sizeof(double _Complex));

    hamiltonian_popcount   = calloc(dim, sizeof(int));
    hamiltonian_A_popcount = calloc(dim_A, sizeof(int));
    hamiltonian_B_popcount = calloc(dim_B, sizeof(int));

    if(!hamiltonian || !hamiltonian_A || !hamiltonian_B || !hamiltonian_spectrum 
        || !rho || !T || !tmp || !rho_A || !rho_A_spectrum
        || !rho_B || !rho_B_spectrum || !hamiltonian_popcount
        || !hamiltonian_A_popcount || !hamiltonian_B_popcount)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }
    start = clock();

    // (2) Setting up Hamiltonian in eigenbasis \hat H|n> = E_n|n>

    report_status("Setting up hamiltonian");
    get_hamiltonian(hamiltonian, hamiltonian_spectrum, length_A + length_B, 0);
    report_status("Setting up hamiltonian (subsystem A)");
    get_hamiltonian(hamiltonian_A, hamiltonian_A_spectrum, length_A, 0);
    report_status("Setting up hamiltonian (subsystem B)");
    get_hamiltonian(hamiltonian_B, hamiltonian_B_spectrum, length_B, 0);

    check_normalized_complex(hamiltonian_A, dim_A);
    check_normalized_complex(hamiltonian_B, dim_B);

    // show_hamiltonian(hamiltonian_A, dim_A, length_A);
    // show_hamiltonian(hamiltonian_B, dim_B, length_B);

    // Shift spectrum to be non-negative to avoid overflow in
    // e^-\beta H for low temperature.

    min_energy = 0.0;
    for(i = 0; i < dim; i++)
    {
        if(hamiltonian_spectrum[i] < min_energy)
        {
            min_energy = hamiltonian_spectrum[i];
        }
    }
    for(i = 0; i < dim; i++)
    {
        hamiltonian_spectrum[i] -= min_energy;
    }

    min_energy = 0.0;
    for(i = 0; i < dim_A; i++)
    {
        if(hamiltonian_A_spectrum[i] < min_energy)
        {
            min_energy = hamiltonian_A_spectrum[i];
        }
    }
    for(i = 0; i < dim_A; i++)
    {
        hamiltonian_A_spectrum[i] -= min_energy;
    }


    min_energy = 0.0;
    for(i = 0; i < dim_B; i++)
    {
        if(hamiltonian_B_spectrum[i] < min_energy)
        {
            min_energy = hamiltonian_B_spectrum[i];
        }
    }
    for(i = 0; i < dim_B; i++)
    {
        hamiltonian_B_spectrum[i] -= min_energy;
    }

    // (3) Count number of particles in the eigenstates,
    //     since we only want half filling.

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            if(hamiltonian[j + i * dim] != 0.0)
            {
                hamiltonian_popcount[i] = __builtin_popcount(j);
                break;
            }
        }
    }

    for(i = 0; i < dim_A; i++)
    {
        for(j = 0; j < dim_A; j++)
        {
            if(hamiltonian_A[j + i * dim_A] != 0.0)
            {
                hamiltonian_A_popcount[i] = __builtin_popcount(j);
                break;
            }
        }
    }

    for(i = 0; i < dim_B; i++)
    {
        for(j = 0; j < dim_B; j++)
        {
            if(hamiltonian_B[j + i * dim_B] != 0.0)
            {
                hamiltonian_B_popcount[i] = __builtin_popcount(j);
                break;
            }
        }
    }

    // (4) Set up initial density matrix 
    // <i|rho(0)|j> = e^(-\beta_A H_A) (x) e^(-\beta_B H_B) / (Z_A Z_B)

    partition_sum_A = 0;
    i = 0;
    for(n_A = 0; n_A < dim_A; n_A++)
    {
        if(hamiltonian_A_popcount[n_A] != length_A/2)
        {
            continue;
        }
        partition_sum_A += exp(-1.0 * beta_A * hamiltonian_A_spectrum[n_A]);
    }

    partition_sum_B = 0;
    i = 0;
    for(n_B = 0; n_B < dim_B; n_B++)
    {
        if(hamiltonian_B_popcount[n_B] != length_B/2)
        {
            continue;
        }
        partition_sum_B += exp(-1.0 * beta_B * hamiltonian_B_spectrum[n_B]);
    }

    start = clock();
    memset(rho_A, '\0', sizeof(double _Complex) * dim_A * dim_A); 

    for(n_A = 0; n_A < dim_A; n_A++)
    {
        snprintf(buf, sizeof(buf), "Calculating rho %i/%i", (int) n_A, (int) dim_A);
        report_status(buf);
        if(hamiltonian_A_popcount[n_A] != length_A/2)
        {
            continue;
        }

        prefactor = exp(-1.0 * beta_A * hamiltonian_A_spectrum[n_A])/partition_sum_A;
        for(col_A = 0; col_A < dim_A; col_A++)
        {
            for(row_A = 0; row_A < dim_A; row_A++)
            {
                rho_A[row_A + dim_A * col_A] += prefactor * hamiltonian_A[row_A + dim_A * n_A]
                                              * hamiltonian_A[col_A + dim_A * n_A];
            }
        }
    }
    snprintf(buf, sizeof(buf), "Calculating rho_A %i/%i", (int) dim_A, (int) dim_A);
    report_status(buf);
    report_elapsed_time();

    start = clock();
    memset(rho_B, '\0', sizeof(double _Complex) * dim_B * dim_B); 

    for(n_B = 0; n_B < dim_B; n_B++)
    {
        snprintf(buf, sizeof(buf), "Calculating rho %i/%i", (int) n_B, (int) dim_B);
        report_status(buf);
        if(hamiltonian_B_popcount[n_B] != length_B/2)
        {
            continue;
        }
        
        prefactor = exp(-1.0 * beta_B * hamiltonian_B_spectrum[n_B])/partition_sum_B;

        for(col_B = 0; col_B < dim_B; col_B++)
        {                     
            for(row_B = 0; row_B < dim_B; row_B++)
            {
                rho_B[row_B + dim_B * col_B] += prefactor * hamiltonian_B[row_B + dim_B * n_B]
                                              * hamiltonian_B[col_B + dim_B * n_B];
            }
        }
    }
    snprintf(buf, sizeof(buf), "Calculating rho_B %i/%i", (int) dim_B, (int) dim_B);
    report_status(buf);
    report_elapsed_time();

    start = clock();
    memset(rho, '\0', sizeof(double _Complex) * dim * dim); 

    for(row_B = 0; row_B < dim_B; row_B++)
    {
        for(row_A = 0; row_A < dim_A; row_A++)
        {
            row_AB = row_A + dim_A * row_B;
            snprintf(buf, sizeof(buf), "Calculating rho = rho_A (x) rho_B %i/%i", (int) row_AB, (int) dim);
            report_status(buf);

            for(col_B = 0; col_B < dim_B; col_B++)
            {
                for(col_A = 0; col_A < dim_A; col_A++)
                {
                    col_AB = col_A + dim_A * col_B;
                    rho[row_AB + dim * col_AB] += rho_A[row_A + dim_A * col_A] * rho_B[row_B + dim_B * col_B];
                }
            }
        }
    }

    snprintf(buf, sizeof(buf), "Calculating rho = rho_A (x) rho_B %i/%i", (int) dim, (int) dim);
    report_status(buf);
    report_elapsed_time();

    check_hermitian(rho, dim);

    // (5) Rotate the density matrix into the eigenbasis of the hamiltonian T_nm = <n|rho(t)|m>.

    start = clock();
    report_status("Rotating rho into hamiltonian eigenbasis");

    double _Complex alpha = 1.0;
    double _Complex beta = 0.0;

    memset(tmp, '\0', sizeof(double _Complex) * dim * dim);
    cblas_zgemm(CblasColMajor, CblasTrans, CblasNoTrans, dim, dim, dim, 
                &alpha, hamiltonian, dim, rho, dim, &beta, tmp, dim);

    memset(T, '\0', sizeof(double _Complex) * dim * dim);
    cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, dim, dim, dim, 
                &alpha, tmp, dim, hamiltonian, dim, &beta, T, dim);
    
    report_elapsed_time();
    
    number_of_steps = (int) ((t_max - t_min)/delta_t + 0.5);

    for(i = 0; i <= number_of_steps; i++)
    {
        t = t_min + i * delta_t;
        printf("\n====== t = %g ======\n", t);

#ifdef STORE_RESULT
        if(find_results())
        {
            printf("  Skipping \n");
            continue;
        }
#endif

        // (6) Calculate time evolved density matrix
        //     <i|rho(t)|j> = \sum <i|n><n|U(t)|n><n|rho(0)|m><m|U^\dagger(t)|m><m|j>
        //                  = \sum e^-it(E_n-E_m) <i|n> T_nm <m|j> 
        start = clock();
        snprintf(buf, sizeof(buf), "Calculating rho(t=%g)", t);
        report_status(buf);

        // Step 1: rho_nm = e^-it(E_n-E_m) T_nm
        memset(rho, '\0', sizeof(double _Complex) * dim * dim);
        for(m = 0; m < dim; m++)
        {
            for(n = 0; n < dim; n++)
            {
                prefactor = cexp(-1.0 * _Complex_I * t * (hamiltonian_spectrum[n] - hamiltonian_spectrum[m]));
                rho[n + dim * m] = prefactor * T[n + dim * m];
            }
        }

        // Step 2: tmp_im = <i|n> rho_nm
        memset(tmp, '\0', sizeof(double _Complex) * dim * dim);
        cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, dim, dim, dim, 
                &alpha, hamiltonian, dim, rho, dim, &beta, tmp, dim);


        // Step 3: rho_ij = tmp_im <m|j>
        memset(rho, '\0', sizeof(double _Complex) * dim * dim);
        cblas_zgemm(CblasColMajor, CblasNoTrans, CblasTrans, dim, dim, dim, 
                    &alpha, tmp, dim, hamiltonian, dim, &beta, rho, dim);

        report_elapsed_time();

        // (7) Calculating partial trace rho_A(t) = Tr_B rho(t) 

        start = clock();
        memset(rho_A, '\0', sizeof(double _Complex) * dim_A * dim_A);
        memset(rho_A_spectrum, '\0', sizeof(double) * dim_A);

        for(diag_B = 0; diag_B < dim_B; diag_B++)
        {
            for(row_A = 0; row_A < dim_A; row_A++)
            {
                row_AB = row_A + dim_A * diag_B;
                for(col_A = 0; col_A < dim_A; col_A++)
                {
                    col_AB = col_A + dim_A * diag_B;
                    rho_A[row_A + dim_A * col_A] += rho[row_AB + dim * col_AB];
                }
            }
            snprintf(buf, sizeof(buf), "Partial Trace rho -> rho_A %i/%i", (int) (row_A+1), (int) dim_A);
            report_status(buf);
        }
        report_elapsed_time();

        check_hermitian(rho_A, dim_A);

        E_A = 0.0;
        N_A = 0.0;
        N_A_err = 0.0;

        fprintf(output_file, "%g ", t);
        for(j = 0; j < length_A; j++)
        {
            double _Complex weight = 0.0;
            for(diag_A = 0; diag_A < dim_A; diag_A++)
            {
                if(diag_A & (1<<j))
                {
                    weight += rho_A[diag_A + dim_A * diag_A];
                }
            }
            if(fabs(cimag(weight)) > 1E-12)
            {
                fprintf(stderr, "Found complex diagonal elements.");
                exit(EX_SOFTWARE);
            }
            fprintf(output_file, "%g ", creal(weight));
        }

        for(row_A = 0; row_A < dim_A; row_A++)
        {
            for(col_A = 0; col_A < dim_A; col_A++)
            {
                for(n_A = 0; n_A < dim_A; n_A++)
                {
                    double weight = rho_A[row_A + dim_A * col_A] * hamiltonian_A[row_A + dim_A * n_A]
                                    * hamiltonian_A[col_A + dim_A * n_A];
                    E_A += hamiltonian_A_spectrum[n_A] *  weight;
                    N_A += hamiltonian_A_popcount[n_A] * weight;
                    N_A_err += pow(hamiltonian_A_popcount[n_A], 2.0) * weight;
                }
            }
        }
        N_A_err = sqrt(N_A_err - pow(N_A, 2.0));

        printf("E_A = %g\n", E_A);
        printf("N_A = %g ± %g\n", N_A, N_A_err);

        start = clock();
        report_status("Diagonalizing rho_A");
        diagonalize_hermitian(rho_A, dim_A, rho_A_spectrum);


        S_A = calculate_vonNeumann_entropy(rho_A_spectrum, dim_A);
        printf("S_A = %g\n", S_A);


        // (7) Calculating partial trace rho_B(t) = Tr_A rho(t) 

        start = clock();
        memset(rho_B, '\0', sizeof(double _Complex) * dim_B * dim_B);
        memset(rho_B_spectrum, '\0', sizeof(double) * dim_B);

        for(row_B = 0; row_B < dim_B; row_B++)
        {
            for(diag_A = 0; diag_A < dim_A; diag_A++)
            {
                row_AB = diag_A + dim_A * row_B;
                for(col_B = 0; col_B < dim_B; col_B++)
                {
                    col_AB = diag_A + dim_A * col_B;
                    rho_B[row_B + dim_B * col_B] += rho[row_AB + dim * col_AB];
                }
            }
            snprintf(buf, sizeof(buf), "Partial Trace rho -> rho_B %i/%i", (int) (row_B+1), (int) dim_B);
            report_status(buf);
        }
        report_elapsed_time();

        check_hermitian(rho_B, dim_B);

        E_B = 0.0;
        N_B = 0.0;
        N_B_err = 0.0;

        for(j = 0; j < length_B; j++)
        {
            double _Complex weight = 0.0;
            for(diag_B = 0; diag_B < dim_B; diag_B++)
            {
                if(diag_B & (1<<j))
                {
                    weight += rho_B[diag_B + dim_B * diag_B];
                }
            }
            if(fabs(cimag(weight)) > 1E-12)
            {
                fprintf(stderr, "Found complex diagonal elements.");
                exit(EX_SOFTWARE);
            }
            fprintf(output_file, "%g ", creal(weight));
        }
        fprintf(output_file, "\n");

        for(row_B = 0; row_B < dim_B; row_B++)
        {
            for(col_B = 0; col_B < dim_B; col_B++)
            {
                for(n_B = 0; n_B < dim_B; n_B++)
                {
                    double weight = rho_B[row_B + dim_B * col_B] * hamiltonian_B[row_B + dim_B * n_B]
                                    * hamiltonian_B[col_B + dim_B * n_B];
                    E_B += hamiltonian_B_spectrum[n_B] * weight;
                    N_B += hamiltonian_B_popcount[n_B] * weight;
                    N_B_err += pow(hamiltonian_B_popcount[n_B], 2.0) * weight;
                }
            }
        }
        N_B_err = sqrt(N_B_err - pow(N_B, 2.0));

        printf("E_B = %g\n", E_B);
        printf("N_B = %g ± %g\n", N_B, N_B_err);


        start = clock();
        report_status("Diagonalizing rho_B");
        diagonalize_hermitian(rho_B, dim_B, rho_B_spectrum);


        S_B = calculate_vonNeumann_entropy(rho_B_spectrum, dim_B);
        printf("S_B = %g\n", S_B);

#ifdef STORE_RESULT
        // (8) Store results in SQLite Database
        store_results();
#endif

    }

    // (9) Deallocation

    flock(fileno(output_file), LOCK_UN);
    fclose(output_file);

    free(hamiltonian_B_popcount);
    free(hamiltonian_A_popcount);
    free(hamiltonian_popcount);
    free(rho_B_spectrum);
    free(rho_B);
    free(rho_A_spectrum);
    free(rho_A);
    free(tmp);
    free(T);
    free(rho);
    free(hamiltonian_B_spectrum);
    free(hamiltonian_A_spectrum);
    free(hamiltonian_spectrum);
    free(hamiltonian_B);
    free(hamiltonian_A);
    free(hamiltonian);

    start = init;
    report_elapsed_time();

    return 0;
}


void get_hamiltonian(double _Complex *H, double *H_spectrum, int L, int periodic_boundary)
{
    char buf[1024];

    int i,j;
    int x,y,z,m,n;
    double factor;
    int dim;

    int H_block_count;
    int *H_block_structure = 0;
    int H_block_index;
    double _Complex *H_block;
    double *H_block_spectrum;
    lapack_int H_block_size;


    // (0) Setup Hamiltonian in Position basis |i>

    dim = 1 << L;

    memset(H, '\0', sizeof(double) * dim * dim);

    for(n = 0; n < dim; n++)
    {
        for(x = 0; x < L; x++)
        {
            if(!(n & (1 << x)))
            {
                continue;
            }
            // x is occupied
            y = x + 1;

            /* Next Nearest Neighbor Hopping */
            z = x + 2;

            factor = 1.0;

            if(periodic_boundary)
            {
                if(y == L) // |y----x| -> |yz----| We jump over all particles between y and x!
                {
                    y = 0;
                    z = z - L;
                    
                    for(i = y+1; i < x; i++)
                    {
                        if(n & (1 << i)) {
                            factor = factor * -1.0;
                        }
                    }
                }
                else if(z == L) // |----xy| -> |z----y| We jump over all particles left of x
                {
                    z = z - L;
                    
                    for(i = 0; i < x; i++)
                    {
                        if(n & (1 << i)) {
                            factor = factor * -1.0;
                        }
                    }
                }
                else if(n & (1 << y)) // We jump over y
                {
                    factor = factor * -1.0;
                }

                if(!(n & (1 << z)))
                {
                    m = n + (1 << z) - (1 << x);
                    #ifdef HARDCORE_BOSONS
                    H[n + dim*m] += -0.5 * lambda;
                    H[m + dim*n] += -0.5 * lambda;
                    #else
                    H[n + dim*m] += -0.5 * lambda * factor;
                    H[m + dim*n] += -0.5 * lambda * factor;
                    #endif
                }
            }
            else
            {
                if(z < L)
                {
                    if(n & (1 << y)) // We jump over y
                    {
                        factor = factor * -1.0;
                    }

                    if(!(n & (1 << z)))
                    {
                        m = n + (1 << z) - (1 << x);
                        #ifdef HARDCORE_BOSONS
                        H[n + dim*m] += -0.5 * lambda;
                        H[m + dim*n] += -0.5 * lambda;
                        #else
                        H[n + dim*m] += -0.5 * lambda * factor;
                        H[m + dim*n] += -0.5 * lambda * factor;
                        #endif
                    }
                }
            }
            /* End Next Nearest Neighbor Hopping*/


            y = x + 1;
    
            if(y == L)
            {
                if(periodic_boundary)
                {
                    y = 0;
                    factor = -1.0;
                }
                else
                {
                    continue;
                }
            }
            else
            {
                factor = 1.0;
            }


            if(n & (1 << y)) {
                // x and y are occupied!

                /* Interaction Term */
                H[n + dim * n] += V;
                continue;
            }

            // x is occupied
            // y is unoccupied

            m = n + (1 << y) - (1 << x);

            // If a particle moves through the periodic boundary and
            // the N-1 state has an odd number of particles, we get
            // an extra sign because we need to commute with all
            // N-1 fermionic ladder operators!

            if((__builtin_popcount(m)%2) == 0)
            {
                #ifdef HARDCORE_BOSONS
                H[n + dim*m] += -0.5;
                H[m + dim*n] += -0.5;
                #else
                H[n + dim*m] += -0.5 * factor;
                H[m + dim*n] += -0.5 * factor;
                #endif
            }
            else
            {
                H[n + dim*m] += -0.5;
                H[m + dim*n] += -0.5;
            }
        }
        snprintf(buf, sizeof(buf), "Setting up Hamiltonian %i/%i", n+1, (int) dim);
        report_status(buf);
    }
    report_elapsed_time();

    // show_hamiltonian(H, dim, L);
    check_hermitian(H, dim);

    // (1) Find symmetry blocks of the block-diagonal Hamiltonian

    H_block_structure = calloc(dim * dim, sizeof(int));

    H_block_count = find_blocks(H, H_block_structure, dim);
    printf("\nThe Hamiltonian has %i blocks.\n", H_block_count);

    // (2) For each symmetry block
    //     (a) Extract block into H_block
    //     (b) Diagonalize H_block
    //     (c) Write back diagonalized block into full hamiltonian matrix
    //     (d) Write spectrum of H_block into full hamiltonian spectrum

    for(H_block_index = 1; H_block_index <= H_block_count; H_block_index++)
    {
        H_block_size = 0;
        for(i = 0; i < dim; i++)
        {
            if(H_block_structure[i] == H_block_index)
            {
                H_block_size++;
            }
        }

        H_block = calloc(H_block_size * H_block_size, sizeof(double _Complex));
        H_block_spectrum = calloc(H_block_size, sizeof(double));

        x = 0;
        for(i = 0; i < dim; i++)
        {
            while(i < dim && H_block_structure[i] != H_block_index)
            {
                i++;
            }
            if(i >= dim)
            {
                break;
            }

            y = 0;
            for(j = 0; j < dim; j++)
            {
                while(j < dim && H_block_structure[j] != H_block_index)
                {
                    j++;
                }
                if(j >= dim)
                {
                    break;
                }

                H_block[x + H_block_size * y] = H[i + dim * j];
                y++;
            }
            x++;
        }

        snprintf(buf, sizeof(buf), "Diagonalizing hamiltonian block %i (%ix%i)", 
                 H_block_index, (int) H_block_size, (int) H_block_size);
        report_status(buf);
        diagonalize_hermitian_eigenvectors(H_block, H_block_size, H_block_spectrum);

        x = 0;
        for(i = 0; i < dim; i++)
        {
            while(i < dim && H_block_structure[i] != H_block_index)
            {
                i++;
            }
            if(i >= dim)
            {
                break;
            }
            y = 0;

            for(j = 0; j < dim; j++)
            {
                while(j < dim && H_block_structure[j] != H_block_index)
                {
                    j++;
                }
                if(j >= dim)
                {
                    break;
                }
                H[i + dim * j] = H_block[x + H_block_size * y];
                y++;
            }
            H_spectrum[i]  = H_block_spectrum[x];
            x++;
        }

        free(H_block_spectrum);
        free(H_block);
    }
    free(H_block_structure);
    report_elapsed_time();
}


int find_blocks(double _Complex *H, int *block_structure, long long int size)
{
    int row, index;
    memset(block_structure, '\0', size * sizeof(int));

    index = 1;
    for(row = 0; row < size; row++)
    {
        if(block_structure[row] == 0)
        {
            find_blocks_recursion(H, block_structure, size, row, index);
            index++;
        }
    }
    return index - 1;
}


void find_blocks_recursion(double _Complex *H, int *block_structure, 
    long long int size, int row, int index)
{
    int col;
    block_structure[row] = index;

    for(col = 0; col < size; col++)
    {
        if(cabs(H[size*row + col]) > 1E-10 && block_structure[col] == 0)
        {
            find_blocks_recursion(H, block_structure, size, col, index);
        }
    }
}


#ifdef STORE_RESULT
int find_results(void)
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;

    int count;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\n128: Can't open database: %s\n", sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(lambda real, V real, L_A integer, beta_A real, L_B integer, beta_B real, t real, "
        "S_A real, E_A real, N_A real, N_A_err real, "
        "S_B real, E_B real, N_B real, N_B_err real, "
        "version TEXT, CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(lambda, V, L_A, beta_A, L_B, beta_B, t))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n129: SQL error %i: %s\n", rc, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM " TABLENAME " WHERE "
        "lambda = ?1 AND V = ?2 AND L_A = ?3 AND beta_A = ?4 "
        "AND L_B = ?5 AND beta_B = ?6 AND t = ?7;", -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 1, lambda);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, V);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 3, length_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 4, beta_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 5, length_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 6, beta_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 7, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_ROW)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
    
    count = sqlite3_column_int(stmt, 0);

    rc = sqlite3_reset(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    return count > 0;
}


void store_results()
{
    // Sqlite3 
    sqlite3 *db;
    int rc;
    char *zErrMsg = 0;
    sqlite3_stmt *stmt;

    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\n107: Can't open database: %s\n", sqlite3_errmsg(db));
        exit(EX_CANTCREAT);
    }

    sqlite3_busy_timeout(db, 10000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(lambda real, V real, L_A integer, beta_A real, L_B integer, beta_B real, t real, "
        "S_A real, E_A real, N_A real, N_A_err real, "
        "S_B real, E_B real, N_B real, N_B_err real, "
        "version TEXT, CONSTRAINT " TABLENAME "_uniq UNIQUE "
        "(lambda, V, L_A, beta_A, L_B, beta_B, t))", NULL, 0, &zErrMsg);

    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, 
        "INSERT INTO " TABLENAME " (lambda, V, L_A, beta_A, L_B, beta_B, t, "
        "S_A, E_A, N_A, N_A_err, S_B, E_B, N_B, N_B_err, version) " 
        "VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16);", 
        -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 1, lambda);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 2, V);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 3, length_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 4, beta_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_int(stmt, 5, length_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 6, beta_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 7, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 8, S_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 9, E_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 10, N_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 11, N_A_err);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 12, S_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 13, E_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 14, N_B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt, 15, N_B_err);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_text(stmt, 16, GIT_DESCRIBE, sizeof(GIT_DESCRIBE), SQLITE_STATIC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt);
    if(rc != SQLITE_OK && rc != SQLITE_DONE) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        if(rc == SQLITE_CONSTRAINT)
        {
            sqlite3_finalize(stmt);
            sqlite3_close(db);
            return;
        }
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\nSQL error %i in %s:%i: %s\n", rc, __FILE__, __LINE__, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
}
#endif
