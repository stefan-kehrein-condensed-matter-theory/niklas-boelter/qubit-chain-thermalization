#include "util.h"
#include "show.h"
#include "sha-256.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <string.h>
#include <sysexits.h>

extern inline uint32_t fallback_pext_u32(uint32_t value, uint32_t mask);
extern inline uint32_t fallback_pdep_u32(uint32_t value, uint32_t mask);
extern inline uint64_t fallback_pext_u64(uint64_t value, uint64_t mask);
extern inline uint64_t fallback_pdep_u64(uint64_t value, uint64_t mask);

clock_t start;
int argv_len; // Used for status reporting
char *argv_ptr;
char *status_prefix; // Prefix string for status reporting

double calculate_Renyi_entropy(const double *eigenvalues, int M, double alpha)
{
    int i;
    double sum = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;

    if(alpha == INFINITY)
    {
        return calculate_min_entropy(eigenvalues, M);
    }
    else if(alpha == 1.0)
    {
        return calculate_vonNeumann_entropy(eigenvalues, M);
    }

    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum += pow(eigenvalues[i], alpha);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    return log2(sum)/(1-alpha);
}


double calculate_vonNeumann_entropy(const double *eigenvalues, int M)
{
    int i;
    double SvN = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            SvN = SvN - eigenvalues[i]*log2(eigenvalues[i]);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
       exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
       exit(EX_SOFTWARE);
    }
    return SvN;
}


double calculate_min_entropy(const double *eigenvalues, int M)
{
    int i;
    double max_eigenvalue = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }

        if(eigenvalues[i] > max_eigenvalue)
        {
            max_eigenvalue = eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    return -1.0 * log2(max_eigenvalue);
}


int calculate_momentum(long long x, int L)
{
    int i = 0;
    int sum = 0;
    while(x)
    {
        if(x & 1) 
        {   
            sum = sum + i;
        }
        x = x >> 1;
        i++;
    }
    return sum % L;
}


int parity_transformation(long long x, int L)
{
    int i;
    int y = 0;
    for(i = 0; i < L; i++)
    {
        if(x & (1<<i))
        {
            y = y | (1<<((L - i)%L));
        }
    }
    return y;
}


double inverse_participation_ratio(const double *state, long long dim, long long stride)
{
    int i;
    double result = 0.0;
    for(i = 0; i < dim; i++)
    {
        result += pow(state[i*stride], 4.0);
    }
    return result;
}



double participation_Renyi_entropy(const double *state, long long dim, 
    long long stride, double alpha)
{
    int i;
    double result;
    double *participations = calloc(dim, sizeof(double));
    for(i = 0; i < dim; i++)
    {
        participations[i] = pow(state[i*stride], 2.0);
    }
    result = calculate_Renyi_entropy(participations, (int) dim, alpha);
    free(participations);
    return result;
}

static int compare_double(const void *a, const void *b) {
    double x = *(const double *) a;
    double y = *(const double *) b;
    if (x < y)
    {
        return -1;
    }
    else if (x > y)
    {
        return 1;
    }
    return 0;
}

double consecutive_level_spacing_ratio(const double *H_eigenvalues, const int *H_popcount,
    long long dim, int filling)
{
    int i;
    int length = 0;
    double result = 0.0;
    double *eigenvalues_sorted = calloc(dim, sizeof(double));

    // We only want specific filling / magnetization

    for(i = 0; i < dim; i++)
    {
        if(H_popcount[i] == filling)
        {
            eigenvalues_sorted[length++] = H_eigenvalues[i];
        }
    }

    qsort(eigenvalues_sorted, length, sizeof(double), compare_double);

    if(length < 3)
    {
        return NAN;
    }
    
    printf("\n");
    for(i = 2; i < length; i++)
    {
        result += min(eigenvalues_sorted[i] - eigenvalues_sorted[i-1], 
                    eigenvalues_sorted[i-1] - eigenvalues_sorted[i-2])
                / max(eigenvalues_sorted[i] - eigenvalues_sorted[i-1],
                    eigenvalues_sorted[i-1] - eigenvalues_sorted[i-2]);
    }
    free(eigenvalues_sorted);
    return result / (length-2);

}


/* 
 * After finding a serious bug in the lapacke interface I decided to call LAPACK directly
 */

/*
void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues)
{
    lapack_int ret;
    start = clock();

    ret = LAPACKE_zgeev(LAPACK_ROW_MAJOR, 'N', 'N', dim, M, dim, eigenvalues, NULL, 1, NULL, 1);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    // report_elapsed_time();
}


void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_zheev(LAPACK_ROW_MAJOR, 'V', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    report_elapsed_time();
}


void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_zheev(LAPACK_ROW_MAJOR, 'N', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'V', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues)
{
    lapack_int ret;

    start = clock();

    ret = LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'N', 'U', dim, M, dim, eigenvalues);

    if(ret)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}
*/

void diagonalize(double _Complex *M, lapack_int dim, double _Complex *eigenvalues)
{
    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;
    lapack_int dvr = 1;

    start = clock();

    rwork = malloc(2 * dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, &workopt, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 4
#endif
    );

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, work, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 4
#endif
    );

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_hermitian_eigenvectors(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    rwork = malloc(3 * dim * sizeof(double));

    memset(eigenvalues, 0, dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 7, 5
#endif
    );

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 7, 5
#endif
    );

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_hermitian(double _Complex *M, lapack_int dim, double *eigenvalues)
{
    /* Lapacke variables */
    lapack_int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    rwork = malloc(3 * dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 5
#endif
    );

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 5
#endif
    );

    free(rwork);
    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric_eigenvectors(double *M, lapack_int dim, double *eigenvalues)
{
    /* Lapacke variables */
    lapack_int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    lwork = -1;
    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 7, 5
#endif
    );

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 7, 5
#endif
    );

    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric(double *M, lapack_int dim, double *eigenvalues)
{
    /* Lapacke variables */
    lapack_int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    lwork = -1;
    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 5
#endif
    );

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Workspace query failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret
#ifdef LAPACK_FORTRAN_STRLEN_END
        , 4, 5
#endif
    );

    free(work);

    if((int)ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed. Return Code: %i\n", (int) ret);
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void report_status(const char *status)
{
    char buf[2048];

    memset(buf, '\0', 2048);

    if(status_prefix)
    {
        snprintf(buf, sizeof(buf), "%s%s", status_prefix, status);
    }
    else
    {
        snprintf(buf, sizeof(buf), "%s", status);
    }

    if(argv_ptr)
    {
        memset(argv_ptr, '\0', argv_len);
        snprintf(argv_ptr, argv_len, "%s", buf);
    }
    printf("\r%s", buf);
    fflush(stdout);
}


void report_elapsed_time()
{
    clock_t stop = clock();
    long ms = (stop - start) * 1000 / CLOCKS_PER_SEC;

    if(ms > 1000*60*60)
    {
        printf("# Time taken: %4ld hours %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else if(ms > 1000*60)
    {
        printf("# Time taken: %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else
    {
        printf("# Time taken: %2ld seconds %4ld milliseconds\n",
        (ms/1000)%60, ms%1000);
    }
    
}


void print_version(void)
{
    lapack_int vers_major;
    lapack_int vers_minor;
    lapack_int vers_patch;

    printf("\n");

    ilaver_(&vers_major, &vers_minor, &vers_patch);
    printf("LAPACK version %i.%i.%i\n", (int) vers_major, (int) vers_minor, (int) vers_patch);

#ifdef USE_LIBFLAME
    printf("%s", FLA_Get_AOCL_Version());
#endif

#ifdef USE_INTEL_MKL
    MKLVersion Version;

    mkl_get_version(&Version);
    printf("MKL version %d.%d.%d build %s\n", Version.MajorVersion, Version.MinorVersion, Version.UpdateVersion, Version.Build);
    printf(" * %s\n", Version.Processor);
#endif

    printf("\n\nVersion %s (compiler version %s)\n", GIT_DESCRIBE, __VERSION__);

}


void check_normalized(const double *V, long long N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += V[i] * V[i];
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized!\n");
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_normalized_complex(const double _Complex *V, long long N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += creal(V[i] * conj(V[i]));
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized! %g \n", squaredNorm);
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_unitary(const double _Complex *U, long long N)
{
    int i,j,k;
    double _Complex matrixElement;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            matrixElement = 0.0 + 0.0 * _Complex_I;
            for(k = 0; k < N; k++)
            {
                matrixElement += U[i + N * k] * conj(U[j + N * k]);
            }

            if((i == j && cabs(matrixElement - 1.0) > 1E-12) || 
               (i != j && cabs(matrixElement) > 1E-12))
            {
                fprintf(stderr, "\nError: O is not unitary!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nUnitary ✔︎\n");
}


void check_hermitian(const double _Complex *rho, long long N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(cabs(rho[i + N * j] - conj(rho[j + N * i])) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not hermitian!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nHermitian ✔︎\n");
}


void check_symmetric(const double *rho, long long N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(fabs(rho[i + N * j] - rho[j + N * i]) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not symmetric!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nSymmetric ✔︎\n");
}


void show_checksum(const void *data, size_t size)
{
    int i;
    uint8_t hash[32];
    char hash_string[65];

    char *hash_string_ptr = &(hash_string[0]);

    start = clock();

    calc_sha_256(hash, data, size);

    for (i = 0; i < 32; i++) {
        hash_string_ptr += sprintf(hash_string_ptr, "%02x", hash[i]);
    }
    
    printf("Checksum  %s  ", hash_string);
    report_elapsed_time();
}


double find_temperature(double *hamiltonian_spectrum, int dim, double energy)
{
    int k;

    double effective_beta_left = -1.0;
    double effective_beta_right = 1.0;

    if(fabs(energy) < 1E-13)
    {
        return INFINITY;
    }

    // We first find a beta large enough such that <H>_equilibrium(beta) < <H>
    while(1)
    {
        double effective_beta_energy = 0.0;
        double effective_beta_partitionSum = 0.0;

        for(k = 0; k < dim; k++)
        {
            effective_beta_partitionSum += exp(-1.0 * effective_beta_right * hamiltonian_spectrum[k]);
            effective_beta_energy += hamiltonian_spectrum[k] * exp(-1.0 * effective_beta_right * hamiltonian_spectrum[k]);
        }
        effective_beta_energy /= effective_beta_partitionSum;

        if(effective_beta_energy > energy)
        {
            effective_beta_left = effective_beta_right;
            effective_beta_right = effective_beta_right * 2.0;
        }
        else
        {
            break;
        }
    }

    // We next find a beta small enough such that <H>_equilibrium(beta) > <H>
    while(1)
    {
        double effective_beta_energy = 0.0;
        double effective_beta_partitionSum = 0.0;

        for(k = 0; k < dim; k++)
        {
            effective_beta_partitionSum += exp(-1.0 * effective_beta_left * hamiltonian_spectrum[k]);
            effective_beta_energy += hamiltonian_spectrum[k] * exp(-1.0 * effective_beta_left * hamiltonian_spectrum[k]);
        }
        effective_beta_energy /= effective_beta_partitionSum;

        if(effective_beta_energy < energy)
        {
            effective_beta_right = effective_beta_left;
            effective_beta_left = effective_beta_left * 2.0;
        }
        else
        {
            break;
        }
    }

    // Now we do a binary search for <H>_equilibrium(beta) = <H>_after_quench
    int effective_beta_runs = 0;
    while( fabs(effective_beta_left - effective_beta_right) > 1E-12 || effective_beta_runs > 100)
    {
        effective_beta_runs++;
        double effective_beta_guess = (effective_beta_left + effective_beta_right)/2.0;
        double effective_beta_energy = 0.0;
        double effective_beta_partitionSum = 0.0;

        for(k = 0; k < dim; k++)
        {
            effective_beta_partitionSum += exp(-1.0 * effective_beta_guess * hamiltonian_spectrum[k]);
            effective_beta_energy += hamiltonian_spectrum[k] * exp(-1.0 * effective_beta_guess * hamiltonian_spectrum[k]);
        }
        effective_beta_energy /= effective_beta_partitionSum;
        if(effective_beta_energy > energy)
        {
            effective_beta_left = effective_beta_guess;
        }
        else
        {
            effective_beta_right = effective_beta_guess;
        }
    }
    return (effective_beta_left + effective_beta_right) / 2.0;
}
